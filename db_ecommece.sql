-- MySQL Script generated by MySQL Workbench
-- dom 24 jun 2018 22:04:35 -03
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema dbecommece
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema dbecommece
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `dbecommece` DEFAULT CHARACTER SET utf8 ;
USE `dbecommece` ;

-- -----------------------------------------------------
-- Table `dbecommece`.`usuarios`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dbecommece`.`usuarios` (
  `id_usuario` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(255) NOT NULL,
  `sobrenome` VARCHAR(255) NOT NULL,
  `celular` VARCHAR(255) NOT NULL,
  `telefone` VARCHAR(255) NOT NULL,
  `email` VARCHAR(255) NOT NULL,
  `endereco` VARCHAR(255) NOT NULL,
  `cargo` ENUM('0', '1') NOT NULL,
  `usuario` VARCHAR(255) NOT NULL,
  `password` VARCHAR(255) NOT NULL,
  `password2` VARCHAR(255) NOT NULL,
  `dt_cadastro` DATE NOT NULL,
  `status` ENUM('0', '1') NOT NULL,
  PRIMARY KEY (`id_usuario`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dbecommece`.`categoria`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dbecommece`.`categoria` (
  `id_categoria` INT NOT NULL AUTO_INCREMENT,
  `categoria` VARCHAR(255) NOT NULL,
  `id_usuario` INT NOT NULL,
  PRIMARY KEY (`id_categoria`),
  INDEX `fk_categoria_usuarios_idx` (`id_usuario` ASC),
  CONSTRAINT `fk_categoria_usuarios`
    FOREIGN KEY (`id_usuario`)
    REFERENCES `dbecommece`.`usuarios` (`id_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dbecommece`.`produtos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dbecommece`.`produtos` (
  `id_produto` INT NOT NULL AUTO_INCREMENT,
  `id_categoria` INT NOT NULL,
  `produto` VARCHAR(255) NOT NULL,
  `apresentacao` VARCHAR(255) NOT NULL,
  `unidade` VARCHAR(45) NOT NULL,
  `moeda` VARCHAR(45) NOT NULL,
  `preco_compra` VARCHAR(45) NOT NULL,
  `preco_venda` VARCHAR(45) NOT NULL,
  `estoque` VARCHAR(45) NOT NULL,
  `status` ENUM('0', '1') NOT NULL,
  `imagem` VARCHAR(45) NOT NULL,
  `vencimento` VARCHAR(45) NOT NULL,
  `id_usuario` INT NOT NULL,
  PRIMARY KEY (`id_produto`),
  INDEX `fk_produtos_categoria_idx` (`id_categoria` ASC),
  INDEX `fk_produtos_usuario_idx` (`id_usuario` ASC),
  CONSTRAINT `fk_produtos_categoria`
    FOREIGN KEY (`id_categoria`)
    REFERENCES `dbecommece`.`categoria` (`id_categoria`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_produtos_usuarios`
    FOREIGN KEY (`id_usuario`)
    REFERENCES `dbecommece`.`usuarios` (`id_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = big5;


-- -----------------------------------------------------
-- Table `dbecommece`.`fornecedores`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dbecommece`.`fornecedores` (
  `id_fornecedor` INT NOT NULL AUTO_INCREMENT,
  `cedula` VARCHAR(45) NOT NULL,
  `razao_social` VARCHAR(255) NOT NULL,
  `telefone` VARCHAR(45) NOT NULL,
  `email` VARCHAR(255) NOT NULL,
  `endereco` VARCHAR(255) NOT NULL,
  `dt_cadastro` DATE NOT NULL,
  `status` ENUM('0', '1') NOT NULL,
  `id_usuario` INT NOT NULL,
  PRIMARY KEY (`id_fornecedor`),
  INDEX `fk_fornecedores_usuarios_idx` (`id_usuario` ASC),
  CONSTRAINT `fk_fornecedores_usuarios`
    FOREIGN KEY (`id_usuario`)
    REFERENCES `dbecommece`.`usuarios` (`id_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dbecommece`.`compras`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dbecommece`.`compras` (
  `id_compra` INT NOT NULL AUTO_INCREMENT,
  `dt_compra` DATE NOT NULL,
  `numero_compra` VARCHAR(255) NOT NULL,
  `fornecedor` VARCHAR(255) NOT NULL,
  `cedula_fornecedor` VARCHAR(255) NOT NULL,
  `cliente` VARCHAR(255) NOT NULL,
  `moeda` VARCHAR(255) NOT NULL,
  `subtotal` VARCHAR(45) NOT NULL,
  `total_iva` VARCHAR(45) NOT NULL,
  `total` VARCHAR(45) NOT NULL,
  `tipo_pagamento` VARCHAR(45) NOT NULL,
  `status` ENUM('0', '1') NOT NULL,
  `id_usuario` INT NOT NULL,
  `id_fornecedor` INT NOT NULL,
  PRIMARY KEY (`id_compra`),
  INDEX `fk_compras_usuarios_idx` (`id_usuario` ASC),
  INDEX `fk_compras_fornecedores_idx` (`id_fornecedor` ASC),
  CONSTRAINT `fk_compras_usuarios`
    FOREIGN KEY (`id_usuario`)
    REFERENCES `dbecommece`.`usuarios` (`id_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_compras_fornecedores`
    FOREIGN KEY (`id_fornecedor`)
    REFERENCES `dbecommece`.`fornecedores` (`id_fornecedor`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dbecommece`.`datalhes_compras`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dbecommece`.`datalhes_compras` (
  `id_datalhe_compras` INT NOT NULL,
  `numero_compra` INT NOT NULL,
  `cedula_fornecedor` VARCHAR(255) NOT NULL,
  `id_produto` INT NOT NULL,
  `produto` VARCHAR(255) NOT NULL,
  `moeda` VARCHAR(255) NOT NULL,
  `valor_compra` VARCHAR(255) NOT NULL,
  `qnt_compra` VARCHAR(255) NOT NULL,
  `desconto` VARCHAR(255) NOT NULL,
  `quantidade` VARCHAR(255) NOT NULL,
  `dt_compra` DATE NOT NULL,
  `id_usuario` INT NOT NULL,
  `id_fornecedor` INT NOT NULL,
  `status` ENUM('0', '1') NOT NULL,
  `id_categoria` INT NOT NULL,
  PRIMARY KEY (`id_datalhe_compras`),
  INDEX `fk_datalhes_compras_usuarios_idx` (`id_usuario` ASC),
  INDEX `fk_datalhes_compras_fornecedores_idx` (`id_fornecedor` ASC),
  INDEX `fk_datalhes_compras_categorias_idx` (`id_categoria` ASC),
  INDEX `fk_datalhes_compras_produtos_idx` (`id_produto` ASC),
  CONSTRAINT `fk_datalhes_compras_usuarios`
    FOREIGN KEY (`id_usuario`)
    REFERENCES `dbecommece`.`usuarios` (`id_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_datalhes_compras_fornecedores`
    FOREIGN KEY (`id_fornecedor`)
    REFERENCES `dbecommece`.`fornecedores` (`id_fornecedor`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_datalhes_compras_produtos`
    FOREIGN KEY (`id_produto`)
    REFERENCES `dbecommece`.`produtos` (`id_produto`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_datalhes_compras_categorias`
    FOREIGN KEY (`id_categoria`)
    REFERENCES `dbecommece`.`categoria` (`id_categoria`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dbecommece`.`clientes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dbecommece`.`clientes` (
  `id_cliente` INT NOT NULL AUTO_INCREMENT,
  `cedula_cliente` VARCHAR(255) NOT NULL,
  `nome_cliente` VARCHAR(255) NOT NULL,
  `apelido_cliente` VARCHAR(255) NOT NULL,
  `telefone_cliente` VARCHAR(255) NOT NULL,
  `email_cliente` VARCHAR(255) NOT NULL,
  `endereco_cliente` VARCHAR(255) NOT NULL,
  `dt_cadastro_cliente` VARCHAR(255) NOT NULL,
  `status('0','1')` DATE NOT NULL,
  `id_usuario` INT NOT NULL,
  PRIMARY KEY (`id_cliente`),
  INDEX `fk_clientes_usuarios_idx` (`id_usuario` ASC),
  CONSTRAINT `fk_clientes_usuarios`
    FOREIGN KEY (`id_usuario`)
    REFERENCES `dbecommece`.`usuarios` (`id_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dbecommece`.`vendas`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dbecommece`.`vendas` (
  `id_venda` INT NOT NULL,
  `dt_venda` DATE NOT NULL,
  `numero_venda` VARCHAR(255) NOT NULL,
  `cedula_cliente` VARCHAR(255) NOT NULL,
  `vendedor` VARCHAR(255) NOT NULL,
  `vendascol` VARCHAR(255) NOT NULL,
  `subtotal` VARCHAR(255) NOT NULL,
  `total_iva` VARCHAR(255) NOT NULL,
  `total` VARCHAR(255) NOT NULL,
  `tipo_pagamento` VARCHAR(255) NOT NULL,
  `status` ENUM('0', '1') NOT NULL,
  `id_usuario` INT NOT NULL,
  `id_cliente` INT NOT NULL,
  PRIMARY KEY (`id_venda`),
  INDEX `fk_vendas_usuarios_idx` (`id_usuario` ASC),
  INDEX `fk_vendas_clientes_idx` (`id_cliente` ASC),
  CONSTRAINT `fk_vendas_usuarios`
    FOREIGN KEY (`id_usuario`)
    REFERENCES `dbecommece`.`usuarios` (`id_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_vendas_clientes`
    FOREIGN KEY (`id_cliente`)
    REFERENCES `dbecommece`.`clientes` (`id_cliente`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dbecommece`.`detalhe_vendas`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dbecommece`.`detalhe_vendas` (
  `id_detalhe_venda` INT NOT NULL AUTO_INCREMENT,
  `numero_venda` VARCHAR(255) NOT NULL,
  `cedula_cliente` VARCHAR(255) NOT NULL,
  `id_produto` INT NOT NULL,
  `produto` VARCHAR(255) NOT NULL,
  `moeda` VARCHAR(255) NOT NULL,
  `preco_venda` VARCHAR(255) NOT NULL,
  `desconto` VARCHAR(255) NOT NULL,
  `quantidade` VARCHAR(255) NOT NULL,
  `dt_venda` DATE NOT NULL,
  `id_usuario` INT NOT NULL,
  `id_cliente` INT NOT NULL,
  `status` ENUM('0', '1') NOT NULL,
  PRIMARY KEY (`id_detalhe_venda`),
  INDEX `fk_detalhe_vendas_usuarios_idx` (`id_usuario` ASC),
  INDEX `fk_detalhe_vendas_clientes_idx` (`id_cliente` ASC),
  INDEX `fk_detalhe_vendas_produtos_idx` (`id_produto` ASC),
  CONSTRAINT `fk_detalhe_vendas_usuarios`
    FOREIGN KEY (`id_usuario`)
    REFERENCES `dbecommece`.`usuarios` (`id_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_detalhe_vendas_clientes`
    FOREIGN KEY (`id_cliente`)
    REFERENCES `dbecommece`.`clientes` (`id_cliente`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_detalhe_vendas_produtos`
    FOREIGN KEY (`id_produto`)
    REFERENCES `dbecommece`.`produtos` (`id_produto`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dbecommece`.`empresa`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dbecommece`.`empresa` (
  `id_empresa` INT NOT NULL AUTO_INCREMENT,
  `cedula_empresa` VARCHAR(255) NOT NULL,
  `nome_empresa` VARCHAR(255) NOT NULL,
  `endereco_empresa` VARCHAR(255) NOT NULL,
  `email_empresa` VARCHAR(255) NOT NULL,
  `telefone_empresa` VARCHAR(45) NOT NULL,
  `id_usuario` INT NOT NULL,
  PRIMARY KEY (`id_empresa`),
  INDEX `fk_empresa_usuarios_idx` (`id_usuario` ASC),
  CONSTRAINT `fk_empresa_usuarios`
    FOREIGN KEY (`id_usuario`)
    REFERENCES `dbecommece`.`usuarios` (`id_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dbecommece`.`permissoes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dbecommece`.`permissoes` (
  `id_permissoe` INT NOT NULL,
  `nome` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id_permissoe`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dbecommece`.`permissao_usuario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dbecommece`.`permissao_usuario` (
  `id_permissao_usuario` INT NOT NULL AUTO_INCREMENT,
  `id_usuario` INT NOT NULL,
  `id_permissoe` INT NOT NULL,
  PRIMARY KEY (`id_permissao_usuario`),
  INDEX `fk_permissao_usuario_usuarios_idx` (`id_usuario` ASC),
  INDEX `fk_permissao_usuario_permissoes_idx` (`id_permissoe` ASC),
  CONSTRAINT `fk_permissao_usuario_usuarios`
    FOREIGN KEY (`id_usuario`)
    REFERENCES `dbecommece`.`usuarios` (`id_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_permissao_usuario_permissoes`
    FOREIGN KEY (`id_permissoe`)
    REFERENCES `dbecommece`.`permissoes` (`id_permissoe`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
