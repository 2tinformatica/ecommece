<?php  require_once("header.php"); ?>
  <!--Contenido-->
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">        
        <!-- Main content -->
        <section class="content">
             
          <div id="resultados_ajax"></div>

            <h2>Lista de Usuarios</h2>

            <div class="row">
              <div class="col-md-12">
                  <div class="box">
                    <div class="box-header with-border">
                          <h1 class="box-title">
                            <button class="btn btn-primary btn-lg" id="add_button" onclick="limpar()" data-toggle="modal" data-target="#usuarioModal"><i class="fa fa-plus" aria-hidden="true">&nbsp;</i>Novo Usuário</button></h1>
                        <div class="box-tools pull-right">
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <!-- centro -->
                    <div class="panel-body table-responsive">
                          
                          <table id="usuario_data" class="table table-bordered table-striped">
                            <thead>
                                <tr>         
                                  <th>CPF</th>
                                  <th>Nome</th>
                                  <th>Sobrenome</th>
                                  <th>Usuario</th>
                                  <th>Cargo</th>
                                  <th>Teléfone</th>
                                  <th>Email</th>
                                  <th>Endereço</th>
                                  <th>Última Alteração</th>
                                  <th>Status</th>
                                  <th width="10%">Editar</th>
                                  <th width="10%">Deletar</th>
                                </tr>
                            </thead>

                            <tbody>
                              

                            </tbody>
                          </table>                     
                    </div>                  
                    <!--Fin centro -->
                  </div><!-- /.box -->
              </div><!-- /.col -->
          </div><!-- /.row -->
      </section><!-- /.content -->

    </div><!-- /.content-wrapper -->
  <!--Fin-Contenido-->
    
    <!--FORMULARIO VENTANA MODAL-->

    <div id="usuarioModal" class="modal fade">      
      <div class="modal-dialog">        
         <form method="post" id="usuario_form">
            <div class="modal-content">              
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Adicionar Usuário</h4>                
              </div>

              <div class="modal-body">
                <label>CPF</label>
                  <input type="text" name="cpf" id="cpf" class="form-control" placeholder="CPF" required pattern="[0-9]{0,15}"/> 
              <br />

                <label>Nome</label>
                  <input type="text" name="nome" id="nome" class="form-control" placeholder="Nome" required pattern="^[a-zA-Z_áéíóúñ\s]{0,30}$"/>
              <br />
          
                <label>Sobrenome</label>
                  <input type="text" name="sobrenome" id="sobrenome" class="form-control" placeholder="Sobrenome" required pattern="^[a-zA-Z_áéíóúñ\s]{0,30}$"/>
              <br />
          
                <label>Cargo</label>
                  <select class="form-control" id="cargo" name="cargo" required>
                      <option value="">-- Seleciona cargo --</option>
                      <option value="1" selected>Administrador</option>
                      <option value="0">Estagiário</option>
                  </select>
              <br />
          
                <label>Usuario</label>
                  <input type="text" name="usuario" id="usuario" class="form-control" placeholder="Usuario" required pattern="^[a-zA-Z_áéíóúñ\s]{0,30}$"/>
              <br />
                
                <label>Senha</label>
                  <input type="password" name="password1" id="password1" class="form-control" placeholder="Password" required/>
              <br />
              
                <label>Repita a Senha</label>
                  <input type="password" name="password2" id="password2" class="form-control" placeholder="Repita Password" required/>
              <br />
                
                <label>Telefone</label>
                  <input type="text" name="telefone" id="telefone" class="form-control" placeholder="Teléfono" required pattern="[0-9]{0,15}"/>
              <br />
                
                <label>Email</label>
                  <input type="email" name="email" id="email" class="form-control" placeholder="Email" required="required"/>
              <br />
                
                <label>Endereço</label>
                  <textarea cols="79" rows="3" id="endereco" name="endereco"required pattern="^[a-zA-Z0-9_áéíóúñ°\s]{0,200}$">
                </textarea>
              <br />
                
                <label>Status</label>
                  <select class="form-control" id="status" name="status" required>
                      <option value="">-- Seleciona Status --</option>
                      <option value="1">Ativo</option>
                      <option value="0">Inativo</option>
                  </select>
              </div><!--Fim modal-body-->

              <div class="modal-footer">

                <input type="hidden" name="id_usuario" id="id_usuario"/>

                <button type="submit" name="action" id="btnGuardar" class="btn btn-success pull-left" value="Add"><i class="fa fa-floppy-o" aria-hidden="true">&nbsp;</i>Salvar</button>
         
                <button type="button" onclick="limpar()" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true">&nbsp;</i>Cancelar</button>                 

              </div><!--Fim modal-footer-->

            </div><!--modal-content"-->
           
         </form>

      </div><!--modal-dialog-->

    </div><!--usuarioModal-->

<?php  require_once("footer.php"); ?>

<script type="text/javascript" src="js/usuarios.js"></script>

