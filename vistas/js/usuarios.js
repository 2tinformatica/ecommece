var table;

//Função que ja executa no inicio
function init() {
  listar();

  //quando o botão submit for clicado, então a função guaradaryeditar(e) é executada;
  $('#usuario_form').on('submit', function(e) {
    salvaryeditar(e);
  });

  //mudar o título da janela modal quando o botão é clicado
  $('#add_button').click(function() {
    $('.modal-title').text('Adicionar Usuario');
  });
}

//função que limpa os campos do formulário
function limpar() {
  $('#cpf').val('');
  $('#nome').val('');
  $('#sobrenome').val('');
  $('#cargo').val('');
  $('#usuario').val('');
  $('#password1').val('');
  $('#password2').val('');
  $('#telefone').val('');
  $('#email').val('');
  $('#endereco').val('');
  $('#status').val('');
  $('#id_usuario').val('');
}

//function listar
function listar() {
  tabla = $('#usuario_data')
    .dataTable({
      aProcessing: true, //Ativa o processamento de tabelas
      aServerSide: true, //paginação e filtro realizado pelo servidor
      dom: 'Bfrtip', //Dfinição de elementos de controle da tabela
      buttons: ['copyHtml5', 'excelHtml5', 'csvHtml5', 'pdf'],

      ajax: {
        url: '../ajax/usuario.php?op=listar',
        type: 'get',
        dataType: 'json',
        error: function(e) {
          console.log(e.responseText);
        }
      },

      bDestroy: true,
      responsive: true,
      bInfo: true,
      iDisplayLength: 10, //Por cada 10 registros criar uma página
      order: [[0, 'desc']], //Ordenar (coluna,ordem)

      language: {
        sProcessing: 'Processando...',

        sLengthMenu: 'Mostrar _MENU_ registros',

        sZeroRecords: 'Nem um resultado encontrato',

        sEmptyTable: 'Nem um dado disponivel nessa tabela',

        sInfo: 'Mostrando um total de _TOTAL_ registros',

        sInfoEmpty: 'Mostrando um total de 0 registros',

        sInfoFiltered: '(filtrando de um total de _MAX_ registros)',

        sInfoPostFix: '',

        sSearch: 'Buscar:',

        sUrl: '',

        sInfoThousands: ',',

        sLoadingRecords: 'Carregando...',

        oPaginate: {
          sFirst: 'Primero',

          sLast: 'Último',

          sNext: 'Siguinte',

          sPrevious: 'Anterior'
        },

        oAria: {
          sSortAscending:
            ': Ativar para ordenar a coluna de maneira ascendente',

          sSortDescending:
            ': Ativar para ordenar a coluna de maneira descendente'
        }
      } //linguagem de fechamento
    })
    .DataTable();
}

//Mostrar dados do usuario na janela modal do formulario de edição
function mostrar(id_usuario) {
  $.post('../ajax/usuario.php?op=mostrar', { id_usuario: id_usuario }, function(
    data,
    status
  ) {
    data = JSON.parse(data);

    $('#usuarioModal').modal('show');
    $('#cpf').val(data.cpf);
    $('#nome').val(data.nome);
    $('#sobrenome').val(data.sobrenome);
    $('#cargo').val(data.cargo);
    $('#usuario').val(data.usuario);
    $('#password1').val(data.password1);
    $('#password2').val(data.password2);
    $('#telefone').val(data.telefone);
    $('#email').val(data.email);
    $('#endereco').val(data.endereco);
    $('#status').val(data.status);
    $('.modal-title').text('Editar Usuario');
    $('#id_usuario').val(id_usuario);
    $('#action').val('Edit');
  });
}

//A função salvaryeditar (e); É chamado quando o botão de envio é clicado
function salvaryeditar(e) {
  e.preventDefault(); //A ação padrão do evento não será ativada
  var formData = new FormData($('#usuario_form')[0]);

  var password1 = $('#password1').val();
  var password2 = $('#password2').val();

  //Se a senha corresponder, o formulário será enviado
  if (password1 == password2) {
    $.ajax({
      url: '../ajax/usuario.php?op=salvaryeditar',
      type: 'POST',
      data: formData,
      contentType: false,
      processData: false,

      success: function(datos) {
        console.log(datos);

        $('#usuario_form')[0].reset();
        $('#usuarioModal').modal('hide');

        $('#resultados_ajax').html(datos);
        $('#usuario_data')
          .DataTable()
          .ajax.reload();

        limpar();
      }
    });
  } //Fim da validação
  else {
    bootbox.alert('Senhas não são iguais');
  }
}

//EDITAR ESTADO DEL USUARIO
//importante:id_usuario, est se envia por post via ajax
function alterarStatus(id_usuario, sts) {
  bootbox.confirm('Tem certeza que desa mudar o status?', function(result) {
    if (result) {
      $.ajax({
        url: '../ajax/usuario.php?op=ativarydesativar',
        method: 'POST',

        //pega o valor do id e do estado
        data: { id_usuario: id_usuario, sts: sts },

        success: function(data) {
          $('#usuario_data')
            .DataTable()
            .ajax.reload();
        }
      });
    }
  }); //bootbox
}

init();
