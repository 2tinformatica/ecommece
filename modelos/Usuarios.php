<?php

  //conexao com a base de dados
   require_once("../config/conexao.php");

   class Usuarios extends Conectar {

    //lista de usuarios
   	public function get_usuarios(){

   	  $conectar=parent::conexao();
   	  parent::set_names();

   	  $sql="select * from usuarios";

   	  $sql=$conectar->prepare($sql);
   	  $sql->execute();

   	  return $resultado=$sql->fetchAll();
   	}

    //metodo para registrar usuario
   	public function registrar_usuario($nome,$sobrenome,$cpf,$telefone,$email,$endereco,$cargo,$usuario,$password1,$password2,$status){

      $conectar=parent::conexao();
      parent::set_names();

      $sql="INSERT INTO usuarios VALUES(null,?,?,?,?,?,?,?,?,?,?,now(),now(),?);";

      $sql=$conectar->prepare($sql);

      $sql->bindValue(1, $_POST["nome"]);
      $sql->bindValue(2, $_POST["sobrenome"]);
      $sql->bindValue(3, $_POST["cpf"]);
      $sql->bindValue(4, $_POST["telefone"]);
      $sql->bindValue(5, $_POST["email"]);
      $sql->bindValue(6, $_POST["endereco"]);
      $sql->bindValue(7, $_POST["cargo"]);
      $sql->bindValue(8, $_POST["usuario"]);
      $sql->bindValue(9, $_POST["password1"]);
      $sql->bindValue(10, $_POST["password2"]);
      $sql->bindValue(11, $_POST["status"]);
      $sql->execute();
     
    }//fim metodo registrar usuário

    //metodo para editar usuario
   	public function editar_usuario($id_usuario,$nombre,$apellido,$cedula,$telefono,$email,$direccion,$cargo,$usuario,$password1,$password2,$estado){

      $conectar=parent::conexao();
      parent::set_names();

      $sql="UPDATE usuarios SET 

        nome=?,
        sobrenome=?,
        cpf=?,
        telefone=?,
        email=?,
        endereco=?,
        cargo=?,
        usuario=?,
        password=?,
        password2=?,
        status = ?

        where 
        id_usuario=? ";

      //echo $sql;
      $sql=$conectar->prepare($sql);

      $sql->bindValue(1, $_POST["nome"]);
      $sql->bindValue(2, $_POST["sobrenome"]);
      $sql->bindValue(3, $_POST["cpf"]);
      $sql->bindValue(4, $_POST["telefone"]);
      $sql->bindValue(5, $_POST["email"]);
      $sql->bindValue(6, $_POST["endereco"]);
      $sql->bindValue(7, $_POST["cargo"]);
      $sql->bindValue(8, $_POST["usuario"]);
      $sql->bindValue(9, $_POST["password1"]);
      $sql->bindValue(10, $_POST["password2"]);
      $sql->bindValue(11, $_POST["status"]);
      $sql->bindValue(12,$_POST["id_usuario"]);
      $sql->execute();

      //print_r($_POST);
   	}
      
    //mostrar os dados do usuário pelo id
    public function get_usuario_por_id($id_usuario){
          
      $conectar=parent::conexao();
      parent::set_names();

      $sql="SELECT * FROM usuarios WHERE id_usuario=?";

      $sql=$conectar->prepare($sql);

      $sql->bindValue(1, $id_usuario);
      $sql->execute();

      return $resultado=$sql->fetchAll();

   	}

   	//editar status do uduário, ativar e daseativar status
    public function editar_status($id_usuario,$status){

    	$conectar=parent::conexao();
    	parent::set_names();

      //parametro sts eviado via ajax
   	  if($_POST["sts"]=="0"){
   	    $status=1;
   	  } else {
   	    $status=0;
   	  }

   	  $sql="update usuarios set status=? where id_usuario=?";

   	  $sql=$conectar->prepare($sql);

   	  $sql->bindValue(1,$status);
   	  $sql->bindValue(2,$id_usuario);
   	  $sql->execute();
   	}

    //valida email e cpf do usuario
   	public function get_cpf_email_do_usuario($cpf,$email){
          
      $conectar=parent::conexao();
      parent::set_names();

      $sql="SELECT * FROM usuarios WHERE cpf=? OR email=?";

      $sql=$conectar->prepare($sql);

      $sql->bindValue(1, $cpf);
      $sql->bindValue(2, $email);
      $sql->execute();

      return $resultado=$sql->fetchAll();

   	}
  }



?>