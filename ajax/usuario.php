<?php

  //chamar a conexão do banco de dados
  require_once("../config/conexao.php");

  //chamar modal Usuarios 
  require_once("../modelos/Usuarios.php");

  $usuarios = new Usuarios();
    //Declaramos as variáveis dos valores que são enviados pelo formulário e que recebemos pelo ajax e dizemos que se existe o parâmetro que estamos recebendo
    $id_usuario = isset($_POST["id_usuario"]);
    $nome=isset($_POST["nome"]);
    $sobrenome=isset($_POST["sobrenome"]);
    $cpf=isset($_POST["cpf"]);
    $telefone=isset($_POST["telefone"]);
    $email=isset($_POST["email"]);
    $endereco=isset($_POST["endereco"]); 
    $cargo=isset($_POST["cargo"]);
    $usuario=isset($_POST["usuario"]);
    $password1=isset($_POST["password1"]);
    $password2=isset($_POST["password2"]);
    $status=isset($_POST["status"]);


    switch($_GET["op"]){

      case "salvaryeditar":
        /*Verificamos se o CPF e mail existem no banco de dados, se já existe um registro com o CPF ou mail então o usuário não estará cadastrado*/
        $dados = $usuarios->get_cpf_email_do_usuario($_POST["cpf"],$_POST["email"]);            
        //validação de senha
          if($password1 == $password2){
            /*se o id não existir, registre-o 
            importante: você deve colocar o $_POST se ele não funcionar*/
	          if(empty($_POST["id_usuario"])){

	            /*Se password1 e password2 forem igauis então verificamos se o cpf e mail existem no banco de dados, se já existe um registro com o cpf ou mail então o usuário não estará cadastrado*/
	            if(is_array($dados)==true and count($dados)==0){
                                
                //se o usuário não existe, fazemos os registros
                $usuarios->registrar_usuario($nome,$sobrenome,$cpf,$telefone,$email,$endereco,$cargo,$usuario,$password1,$password2,$status);

                $messagens[]="<div class='alert alert-success' role='alert'><button type='button' class='close' data-dismiss='alert'>&times;</button>
                <strong>Usuário cadastrado com sucesso!</strong>
                </div>";;

              /*Se já houver o e-mail e CPF, a mensagem aparecerá*/
              } else {
                  $messagens[]= "<div class='alert alert-danger' role='alert'><button type='button' class='close' data-dismiss='alert'>&times;</button>
                  <strong>CPF ou Email ja cadastrado!</strong>
                  </div>";
                }
              //fechamento da validação vazia     
	          } else {
                /*se já existe, então nós editamos o usuário*/
                $usuarios->editar_usuario($id_usuario,$nome,$sobrenome,$cpf,$telefone,$email,$endereco,$cargo,$usuario,$password1,$password2,$status);

                $messagens[]= "<div class='alert alert-success' role='alert'><button type='button' class='close' data-dismiss='alert'>&times;</button>
                <strong>Usuário Editado com sucesso!</strong>
                </div>";
	            }                     
          } else {
              /*Se a senha não corresponder, a mensagem de erro será exibida*/
              $errors[]="Senha devem ser iguais";
            }
          //fim verificação do if de senhas
          
          //mensagem de sucesso
          if(isset($messagens)){			           
            foreach($messagens as $messagem) {
              echo $messagem;
            }                  
			    }
	        //fim mensagem de sucesso

          //mensagem de erro
          if(isset($errors)){
            ?>
              <div class="alert alert-danger" role="alert">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                  <strong>Error!</strong> 
                  <?php
                    foreach($errors as $error) {
                        echo $error;
                      }
                    ?>
              </div>
            <?php
          }
          //fin mensagem de erro
        break;

        case "mostrar":
          //seleccionar id do usuario
          //o parâmetro id_usuario é enviado pelo AJAX quando o usuário é editado
          $dados = $usuarios->get_usuario_por_id($_POST["id_usuario"]);

            //validacion del id del usuario  
            if(is_array($dados)==true and count($dados)>0){
             	foreach($dados as $row){      
                $output["cpf"] = $row["cpf"];
                $output["nome"] = $row["nome"];
            		$output["sobrenome"] = $row["sobrenome"];
            		$output["cargo"] = $row["cargo"];
            		$output["usuario"] = $row["usuario"];
            		$output["password1"] = $row["password"];
            		$output["password2"] = $row["password2"];
            		$output["telefone"] = $row["telefone"];
            		$output["email"] = $row["email"];
            		$output["endereco"] = $row["endereco"];
            		$output["status"] = $row["status"];
             	}

             	 echo json_encode($output);

            } else {
                //se o registro não existe, então ele não passa pelo array
                $errors[]="Usuário não existe";
              }
            
              //inicio da mesagem de erro
				    if(isset($errors)){
              ?>
              <div class="alert alert-danger" role="alert">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                  <strong>Error!</strong> 
                  <?php
                    foreach($errors as $error) {
                        echo $error;
                      }
                    ?>
              </div>
              <?php
			      }
	          //fim das mensagem de erro

        break;//fim case mostrar

        case "ativarydesativar":
              
          //os parâmetros id_usuario e est estão chegando via ajax
          $dados = $usuarios->get_usuario_por_id($_POST["id_usuario"]);
                
            //validação do id do usuário
            if(is_array($dados)==true and count($dados)>0){
              //editar status do usuário
              $usuarios->editar_status($_POST["id_usuario"],$_POST["sts"]);
            }
         break;// fim case ativar desativar

         case "listar":
          
          $dados = $usuarios->get_usuarios();
         
          //declaramos o array
          $data = Array();
          
          foreach($dados as $row){            
            $sub_array= array();
              //STATUS
	            $sts = '';
	            $atrib = "btn btn-success btn-md status";
	          if($row["status"] == 0){
	            $sts = 'INATIVO';
	            $atrib = "btn btn-warning btn-md status";
	          }else{
              if($row["status"] == 1){
                $sts = 'ATIVO'; 
              } 
	          }//fim else
            
            //cargo
            if($row["cargo"]==1){
              $cargo="ADMINISTRADOR";
            }else{
            	if($row["cargo"]==0){      
                   $cargo="EMPREGADO";
            	}
            }

          $sub_array[]= $row["cpf"];
          $sub_array[] = $row["nome"];
          $sub_array[] = $row["sobrenome"];
          $sub_array[] = $row["usuario"];
          $sub_array[] = $cargo;
          $sub_array[] = $row["telefone"];
          $sub_array[] = $row["email"];
          $sub_array[] = $row["endereco"];
          $sub_array[] = date("d-m-Y",strtotime($row["dt_cadastro"]));
  
          $sub_array[] = '<button type="button" onClick="alterarStatus('.$row["id_usuario"].','.$row["status"].');" name="status" id="'.$row["id_usuario"].'" class="'.$atrib.'">'.$sts.'</button>';

          $sub_array[] = '<button type="button" onClick="mostrar('.$row["id_usuario"].');"  id="'.$row["id_usuario"].'" class="btn btn-warning btn-md update"><i class="glyphicon glyphicon-edit"></i> Editar</button>';

          $sub_array[] = '<button type="button" onClick="deletar('.$row["id_usuario"].');"  id="'.$row["id_usuario"].'" class="btn btn-danger btn-md"><i class="glyphicon glyphicon-edit"></i> Deletar</button>';
                       
	        $data[]=$sub_array;
 
        }//FIM FOREACHE

          $results= array(	

          "sEcho"=>1, //Información para el datatables
          "iTotalRecords"=>count($data), //enviamos el total registros al datatable
          "iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar
          "aaData"=>$data);
         
           echo json_encode($results);

         break;
     }//FIM switch

?>